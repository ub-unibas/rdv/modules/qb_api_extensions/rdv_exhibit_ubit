#!/usr/bin/python3

import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()


setuptools.setup(
    name="rdv_exhibit_ubit",
    setup_requires=['setuptools-git-versioning'],
    setuptools_git_versioning={
        "enabled": True,
    },
    author="Martin Reisacher, Matthias Edel",
    author_email="martin.reisacher@unibas.ch, matthias.edel@unibas.ch",
    description="extract data from google spreadsheets for rdv vitrinen ",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.switch.ch/ub-unibas/rdv/modules/qb_api_extensions/rdv_exhibit_ubit",
    install_requires=[
                      'flask', 'flask_cors', 'flask_compress', 'flask-restx',  'markupsafe',
                      'requests', 'pygsheets',
                      'cache_decorator_redis_ubit', 'gdspreadsheets_ubit',
                      'rdv_data_helpers_ubit', 'rdv_config_store_ubit'],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    packages=setuptools.find_packages(),
    python_requires=">=3.8",
    include_package_data=True,
    zip_safe=False
)
