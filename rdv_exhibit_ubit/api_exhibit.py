import json

from flask import request
from flask_restx import Resource, Namespace
import pygsheets

from cache_decorator_redis_ubit import CacheDecorator12h, NoCacheDecorator
from gdspreadsheets_ubit import authorize_pyg, PygExtendedClient
from rdv_config_store_ubit import ConfigStore

ns_exhibit = Namespace('content', description="return data for rdv vitrine")


@ns_exhibit.route('/exhibit/<project>/<lang>', methods=['GET'])
@ns_exhibit.route('/exhibit/<project>/<lang>/', methods=['GET'])
class IIIFFeatures(Resource):

    @classmethod
    def get(self, project=None, lang="de"):

        config_store = ConfigStore()
        config_store.load_project_config(project)
        gd_service_var = config_store.get_value('view.gd_service_account_environment_variable')
        gd_service_file = config_store.get_value('view.gd_service_file')
        spreadsheet_id = config_store.get_value('exhibit.spreadsheet')

        PygExtendedClient.gc = authorize_pyg(gd_service_account_env_var=gd_service_var, service_file=gd_service_file, cache_decorator=CacheDecorator12h)
        try:
            spreadsheet = PygExtendedClient.gc.open_by_key(spreadsheet_id)
            sheet = spreadsheet.worksheet_by_title(lang)
            fields_def = sheet.get_all_records()
            return self.prepare_entries(fields_def)
        except pygsheets.exceptions.WorksheetNotFound:
            try:
                print("Language {} for spreadsheet {} did not exist".format(lang, spreadsheet_id))
                spreadsheet = PygExtendedClient.gc.open_by_key(spreadsheet_id)
                sheet = spreadsheet.worksheet_by_title("de")
                fields_def = sheet.get_all_records()
                return self.prepare_entries(fields_def)
            except pygsheets.exceptions.WorksheetNotFound:
                print("Fallback language {} for spreadsheet {} did not exist".format("de", spreadsheet_id))
                return 401

    @staticmethod
    def convert_str2intsort(string):
        try:
            return int(string)
        except ValueError:
            return 9999

    @staticmethod
    def prepare_entries(data):
        return [k for k in sorted(data, key=lambda x: IIIFFeatures.convert_str2intsort(x.get('Reihenfolge', 999))) if k]


